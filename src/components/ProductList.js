import React, { Component } from "react";
import Product from "./Product";
import ProductDatas from "../Data/ProductData";

class ProductList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      products: [],
    };
    this.handleProductUpVote = this.handleProductUpVote.bind(this);
  }
  componentDidMount() {
    this.setState({ products: ProductDatas });
  }
  handleProductUpVote(ProductId) {
    const nextProducts = this.state.products.map((product) => {
      if (product.id === ProductId) {
        return Object.assign({}, product, {
          votes: product.votes + 1,
        });
      } else {
        return product;
      }
    });
    this.setState({
      products: nextProducts,
    });
    console.log(this.state.products);
  }
  render() {
    const SortedProductData = this.state.products.sort(
      (a, b) => b.votes - a.votes
    );
    return (
      <div className="ui unstackable items">
        {SortedProductData.map((ProductData) => (
          <Product
            key={ProductData.id}
            id={ProductData.id}
            title={ProductData.title}
            description={ProductData.description}
            url={ProductData.url}
            votes={ProductData.votes}
            submitterAvatarURL={ProductData.submitterAvatarURL}
            productImageUrl={ProductData.productImageUrl}
            onVote={this.handleProductUpVote}
          />
        ))}
      </div>
    );
  }
}

export default ProductList;
