import React, { Component } from "react";
class Product extends Component {
  onUpVoteHandle = () => {
    this.props.onVote(this.props.id);
  };
  render() {
    return (
      <div className="item">
        <div className="image">
          <img src={this.props.productImageUrl} alt="Product" />
        </div>
        <div className="middle aligned content ">
          <div className="header">
            <button onClick={this.onUpVoteHandle}>
              <i className="large caret up icon" />
            </button>
            {this.props.votes}
          </div>
          <div className="description">
            <a href={this.props.url}>{this.props.title}</a>
            <p>{this.props.description}</p>
          </div>
          <div className="extra">
            <span>Submitted by:</span>
            <img
              className="ui avatar image "
              src={this.props.submitterAvatarURL}
              alt="Avatar"
            ></img>
          </div>
        </div>
      </div>
    );
  }
}

export default Product;
